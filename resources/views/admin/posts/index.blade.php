@extends('admin.admin-layout')

@section('title', 'Lista de Postagens')


@section('content')

<div class="crud posts index">

    @php
        $btns['create']['display'] = true;
        $btns['create']['text'] = 'Cadastrar novo post';
        $btns['create']['route'] = route('posts.create')
    @endphp
    @include('admin.shared.crud-actions-header', [
        'title' => 'Listagem de posts',
        'btns' => $btns
    ])

    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Título</th>
                <th>Editar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts AS $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->title }}</td>
                    <td>
                        <a href="{{ action('Admin\PostsController@edit', ['id' => $post->id]) }}">Editar</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

</div>

@endsection

