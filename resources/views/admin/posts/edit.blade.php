@extends('admin.admin-layout')

@section('title', 'Cadastrar Nova Postagem')

@section('content')

<div class="crud posts edit">

    @php
        $btns['cancel']['display'] = true;
    @endphp
    @include('admin.shared.crud-actions-header', [
        'title' => "Editando post <em>{$post->title}</em>",
        'btns' => $btns
    ])

    <div class="crud-form-wrapper">
        {{-- If I remove this, then it works --}}
        {!! Form::model($post, ['method' => 'PATCH', 'route' => ['posts.update', $post]]) !!}
        @include('admin.posts.form')
        {!! Form::close() !!}
    </div>

</div>

@endsection
