
<div class="form-group">
    {!! Form::label('Categoria', 'Categoria') !!}
    {!! Form::select(
        'post_category_id',
        $post_categories,
        null,
    ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('title', 'Título') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('intro', 'Introdução') !!}
    {!! Form::text('intro', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('text', 'Texto') !!}
    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit('Gravar', ['class' => 'btn btn-primary']) !!}

