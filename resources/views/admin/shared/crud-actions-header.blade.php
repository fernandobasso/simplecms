<header class="crud-header crud-actions">
    <div class="row">
        <h3 class="col-6 h5">{!! $title !!}</h3>
        <div class="col-6 ">
            @if ($btns['cancel']['display'])
            <a class="btn btn-outline-primary float-right"
                role="button"
                href="{{ url()->previous() }}">
                {{ $btns['cancel']['text'] }}
            </a>
            @endif

            @if ($btns['create']['display'])
            <a class="btn btn-outline-primary float-right mr-2"
                role="button"
                href="{{ $btns['create']['route'] }}">
                {{ $btns['create']['text'] }}
            </a>
            @endif
        </div>
    </div>
</header>
