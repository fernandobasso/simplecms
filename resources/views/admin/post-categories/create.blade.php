@extends('admin.admin-layout')

@section('title', 'Cadastrar Nova Categoria de Postagem')

@section('content')

<div class="crud post-categories edit">

    @php
        $btns['cancel']['display'] = true;
    @endphp

    @include('admin.shared.crud-actions-header', [
        'title' => 'Cadastrando nova categoria de post',
        'btns' => $btns
    ])

    <div class="crud-form-wrapper">
        {!! Form::open(['action' => 'Admin\PostCategoriesController@store']) !!}
        @include('admin.post-categories.form', compact('btns'))
        {!! Form::close() !!}
    </div>
</div>

@endsection
