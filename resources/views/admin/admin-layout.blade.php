<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{!! asset('assets/admin/css/bootstrap-custom.css') !!}" />
        <link rel="stylesheet" type="text/css" href="{!! asset('assets/admin/css/main-admin.css') !!}" />
        <title>ADMIN - @yield('title')</title>
    </head>
    <body>


        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">

                <a class="navbar-brand" href="#">SimpleCMS</a>

                {{-- TODO: We should never be in admin without a user, but... --}}
                @if (auth()->user())
                    <div>{{ auth()->user()->name }}
                        {{ auth()->user()->has_role }}
                        <form method="POST" action="{{ route('logout') }}">
                            <input type="submit" value="Sair">
                            {{ csrf_field() }}
                        </form>
                    </div>
                @endif

            </div>
        </nav>

        <div class="container content-body">
            @section('sidebar')
                <section class="main-sidebar">
                    <ul class="nav flex-column">
                        @if (auth()->user()->isAdmin())
                            <li class="nav-item">
                                <a class="nav-link {{ link_active('admin/post-categories') }}"
                                    href="{{ route('post-categories.index') }}">Categorias de Postagens</a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link {{ link_active('admin/posts') }}"
                                href="{{ route('posts.index') }}">Notícias</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('posts.create') }}">Publicar Notícia</a>
                        </li>
                    </ul>
                </section>
            @show

            <div class="container main-container">
                @yield('content')
            </div>
        </div> {{-- .content-body --}}

        <footer class="main-footer">
            <div class="container">
                <p class="mb-0 text-center">Copyright VBS Mídia {{ getCurrentYear() }}</p>
            </div>
        </footer>

    </body>
</html>
