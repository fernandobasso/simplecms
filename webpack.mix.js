let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

mix.sass(
    'resources/assets/admin/scss/main-admin.scss',
    'public/assets/admin/css'
).sourceMaps();

mix.sass(
    'resources/assets/admin/scss/bootstrap-custom.scss',
    'public/assets/admin/css'
).sourceMaps();


