<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Site')->group(function () {
    route::get('/', function () {
        return ['title' => 'Home Page'];
    });
    Route::get('/noticias', 'PostsController@index');
});

Route::namespace('Admin')->prefix('admin')->middleware(['auth', 'admin'])->group(function () {
    Route::get('/', function () {
        return view('admin.welcome');
    })->name('admin.welcome');
    Route::resource('posts', 'PostsController');
    Route::resource('post-categories', 'PostCategoriesController');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
