<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
      'post_category_id',
      'title',
      'intro',
      'text'
    ];

    /**
     * Get PostCategory for this Post.
     */
    public function postCategory()
    {
        return $this->belongsTo('App\PostCategory');
    }
}
