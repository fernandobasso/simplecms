<?php

function getCurrentYear() {
    return (new DateTime('now', new DateTimeZone('America/Sao_Paulo')))->format('Y');
}

function link_active($path) {
    return Request::is($path . '*') ? 'active' : '';
}
