<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PostCategory;
use App\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news_id = PostCategory::where('slug', 'noticias')->get()->first()->id;
        $news = Post::all()->where('post_category_id', $news_id);
        return view('site.news.index', [
            'news' => $news
        ]);
    }
}
